'use strict';

describe('Add Thing', function() {
  var page;

  beforeEach(function() {
    browser.get('/');
    page = require('./main.po');
  });

  it('should appear in the list', function() {
    var inputText = element(by.css("[placeholder='Add a new thing here.']"));
    expect(inputText).toNotEqual(null);

    // expect(page.imgEl.getAttribute('src')).toMatch(/assets\/images\/yeoman.png$/);
    // expect(page.imgEl.getAttribute('alt')).toBe('I\'m Yeoman');
  });
});
